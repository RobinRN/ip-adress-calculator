import java.util.Arrays;

public class Calculator {
    public static void main(String[] args) {
        String ip = args[0];
        int pre = Integer.parseInt(args[1]);
        String[] ipArray = strToIp(ip);
        String[] binIpArray = ipToBinary(ipArray);
        String binIp = ipToStr(binIpArray);

        String[] smaskArray = smask(pre);
        String smask = ipToStr(smaskArray);

        String[] binNetaddrArray = netaddr(binIpArray, pre);
        String binNetaddr = ipToStr(binNetaddrArray);
        String[] netaddrArray = ipToDecimal(binNetaddrArray);
        String netaddr = ipToStr(netaddrArray);

        String[] binBroadaddrArray = broadaddr(binIpArray, pre);
        String binBroadaddr = ipToStr(binBroadaddrArray);
        String[] broadaddrArray = ipToDecimal(binBroadaddrArray);
        String broadaddr = ipToStr(broadaddrArray);

        String[] binSubmaskArray = smask(pre);
        String binSubmask = ipToStr(binSubmaskArray);
        String[] submaskArray = ipToDecimal(binSubmaskArray);
        String submask = ipToStr(submaskArray);

        String[] binMinipArray = minIp(binNetaddrArray);
        String binMinip = ipToStr(binMinipArray);
        String[] minipArray = ipToDecimal(binMinipArray);
        String minip = ipToStr(minipArray);

        String[] binMaxipArray = maxIp(binBroadaddrArray);
        String binMaxip = ipToStr(binMaxipArray);
        String[] maxipArray = ipToDecimal(binMaxipArray);
        String maxip = ipToStr(maxipArray);

        int num = numIps(pre);

        System.out.printf("IP-Adresse: %s = %s%n", ip, binIp);
        System.out.printf("Subnetzmaske: %s = %s%n", submask, binSubmask);
        System.out.printf("Netzadresse: %s = %s%n", netaddr, binNetaddr);
        System.out.printf("Kleinste IP: %s = %s%n", minip, binMinip);
        System.out.printf("Größste IP: %s = %s%n", maxip, binMaxip);
        System.out.printf("Broadcast: %s = %s%n", broadaddr, binBroadaddr);
        System.out.printf("Anzahl möglicher IPs: %d%n", num);
    }

    public static String[] ipToBinary(String[] ip) {
        String[] res = new String[ip.length];
        for (int i = 0; i < ip.length; i++) {
            String binbyte = Converter.convert(ip[i], (byte)10, (byte)2);
            while (binbyte.length() < 8) {
                binbyte = "0" + binbyte;
            }
            res [i] = binbyte;
        }
        return res;
    }

    public static String[] ipToDecimal(String[] ip) {
        String[] res = new String[ip.length];
        for (int i = 0; i < ip.length; i++) {
            String decbyte = Converter.convert(ip[i], (byte) 2, (byte) 10);
            res[i] = decbyte;
        }
        return res;
    }

    public static String ipToStr(String[] ip) {
        return String.join(".", ip);
    }

    public static String[] strToIp(String string) {
        return string.split("\\.");
    }

    public static String[] netaddr(String[] ip, int pre) {
        String ipStr = ipToStr(ip);
        StringBuilder netaddr = new StringBuilder();
        for (int i = 0; i < ipStr.length(); i++) {
            char digit = ipStr.charAt(i);
            if (digit == '.') {
                netaddr.append('.');
                pre++;
                continue;
            }
            if (i < pre) {
                netaddr.append(ipStr.charAt(i));
            } else {
                netaddr.append('0');
            }
        }
        String[] netaddrArray = strToIp(netaddr.toString());
        return netaddrArray;
    }

    public static String[] broadaddr(String[] ip, int pre) {
        String ipStr = ipToStr(ip);
        StringBuilder broadaddr = new StringBuilder();
        for (int i = 0; i < ipStr.length(); i++) {
            char digit = ipStr.charAt(i);
            if (digit == '.') {
                broadaddr.append('.');
                pre++;
                continue;
            }
            if (i < pre) {
                broadaddr.append(ipStr.charAt(i));
            } else {
                broadaddr.append('1');
            }
        }
        String[] broadaddrArray = strToIp(broadaddr.toString());
        return broadaddrArray;
    }

    public static String[] smask(int pre) {
        StringBuilder smask = new StringBuilder();
        for (int i = 0; i < 32; i++) {
            if (i < pre) {
                smask.append('1');
            } else {
                smask.append('0');
            }
            if ((i + 1) % 8 == 0) {
                smask.append('.');
            }
        }
        smask.deleteCharAt(smask.length() - 1);
        String[] smaskArray = strToIp(smask.toString());
        return smaskArray;
    }

    public static String[] minIp(String[] netIp) {
        String[] res = new String[netIp.length];
		res = netIp;
		res[3] = Integer.toString(Integer.parseInt(res[3]) + 1);
        while (res[3].length() < 8) {
            res[3] = "0" + res[3];
        }
        return res;
    }
	
	public static String[] maxIp(String[] broadIp) {
        String[] res = new String[broadIp.length];
		res = broadIp;
		res[3] = Integer.toString(Integer.parseInt(res[3]) - 1);
        while (res[3].length() < 8) {
            res[3] = "0" + res[3];
        }
        return res;
    }
	
	public static int numIps(int pre) {
		return (int) Math.pow(2, 32-pre) - 2;
	}
}
