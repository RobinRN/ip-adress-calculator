public class Converter {
	public static void main(String[] args) {
        if (args.length < 3) {
            throw new IllegalArgumentException("parameters required: 'value' 'initial basis' ''target basis'");
        }
        String value = args[0];
        byte from_basis = (byte) Integer.parseInt(args[1]);
        byte to_basis = (byte) Integer.parseInt(args[2]);
        String res = convert(value, from_basis, to_basis);
        System.out.println(res);
	}

    public static byte digitToDecimal(char digit) {
        digit = Character.toUpperCase(digit);
        int ascii_digit = (int) digit;
        if ('0' <= digit && digit <= '9') {
            return (byte) Character.getNumericValue(digit);
        }
        if ('A' <= digit && digit <= 'Z') {
            return (byte) (ascii_digit - 55);
        }
        throw new IllegalArgumentException("unknown digit");
    }

    public static char decimalToDigit(byte decimal) {
        if (0 <= decimal && decimal <= 9) {
            return Character.forDigit(decimal, 10);
        }
        if (10 <= decimal && decimal <= 35) {
            return (char) (decimal + 55);
        }
        throw new IllegalArgumentException("number is no integer between 0 and 35");
    }

	public static String convert(String number, byte from_basis, byte to_basis) {
        // convert value to decimal
        int decimal = 0;
        for (int i = 0; i < number.length(); i++) {
            char digit = number.charAt(i);
            decimal = decimal * from_basis + digitToDecimal(digit);
        }
        // convert to target basis
        StringBuilder res = new StringBuilder();
        do {
            int quotient = decimal / to_basis;
            byte remainder = (byte) (decimal % to_basis);
            decimal = quotient;
            res.append(decimalToDigit(remainder));
        } while (decimal > 0);
        return res.reverse().toString();
	}
}
