#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 14:17:16 2020

@author: robin
"""

from converter import convert
from stringutils import fill, split_by_index


def ip_to_binary(ip):
    res = []
    for byte in ip:
        binbyte = fill(convert(byte, from_basis=10, to_basis=2))
        res.append(binbyte)
    return res


def ip_to_decimal(ip):
    res = []
    for byte in ip:
        decbyte = convert(byte, from_basis=2, to_basis=10)
        res.append(decbyte)
    return res


def ip_to_str(ip):
    return '.'.join(ip)


def str_to_ip(string):
    return string.split('.')


def netaddr(ip, pre):
    ip_str = ''.join(ip)
    netaddr_str = fill(ip_str[:pre], length=32, left=False)
    netaddr_list = split_by_index(netaddr_str, pos=[8, 16, 24])
    return netaddr_list


def broadaddr(ip, pre):
    ip_str = ''.join(ip)
    broadaddr_str = fill(ip_str[:pre], fill_with='1', length=32, left=False)
    broadaddr_list = split_by_index(broadaddr_str, pos=[8, 16, 24])
    return broadaddr_list


def smask(pre):
    smask_str = fill(pre*'1', length=32, left=False)
    smask_list = split_by_index(smask_str, pos=[8, 16, 24])
    return smask_list


def minip(smask):
    minip = smask[:]
    minip[-1] = fill(int(smask[-1]) + 1, length=8)
    return minip


def maxip(bip):
    maxip = bip[:]
    maxip[-1] = fill(int(bip[-1]) - 1, length=8)
    return maxip


def numip(pre):
    return 2 ** (32-pre) - 2
