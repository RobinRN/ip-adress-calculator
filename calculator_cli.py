#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 16:02:27 2020

@author: robin
"""

import sys
import calculator as c

args = sys.argv
if len(args) < 3:
    print('two parameters required: "ip" "prefix"')
    sys.exit()

ip, pre = c.str_to_ip(args[1]), int(args[2])
bin_ip = c.ip_to_binary(ip)
smask = c.smask(pre)
netaddr = c.netaddr(bin_ip, pre)
broadaddr = c.broadaddr(bin_ip, pre)
minip = c.minip(netaddr)
maxip = c.maxip(broadaddr)
numip = c.numip(pre)

print('Eingabe')
print('-------')
print('IP-Adresse:', c.ip_to_str(ip))
print('Präfix-Länge:', pre)
print()

print('Ausgabe')
print('-------')
print('IP-Adresse:', c.ip_to_str(bin_ip))
print('Subnetz-Maske:', c.ip_to_str(smask))
print('Netzadresse:', c.ip_to_str(netaddr))
print('kleinste IP:', c.ip_to_str(minip))
print('größte IP:', c.ip_to_str(maxip))
print('Broadcast:', c.ip_to_str(broadaddr))
print('Anzahl möglicher IPs:', numip)
