#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 19:18:36 2020

@author: robin
"""


def digit_to_decimal(digit):
    """Convert digit character to decimal integer."""
    digit = digit.upper()
    ascii_digit = ord(digit)
    if '0' <= digit <= '9':
        return int(digit)
    if 'A' <= digit <= 'Z':
        return ascii_digit - 55
    raise ValueError('unknown digit')


def decimal_to_digit(decimal):
    "Convert decimal integer to digit character."
    if 0 <= decimal <= 9:
        return str(decimal)
    if 10 <= decimal <= 35:
        return chr(decimal + 55)
    raise ValueError('number is no integer between 0 and 35.')


def convert(value, from_basis, to_basis):
    """Convert value given in 'basis' to target basis."""
    # convert value to decimal
    decimal = 0
    for digit in value:
        decimal *= from_basis
        decimal += digit_to_decimal(digit)
    # convert to target basis
    res = ''
    while True:
        quotient = decimal // to_basis
        remainder = decimal % to_basis
        decimal = quotient
        res += decimal_to_digit(remainder)
        if decimal == 0:
            break
    return res[::-1]
