#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 20:08:01 2020

@author: robin
"""

from converter import convert
import sys

args = sys.argv
if len(args) < 4:
    print('three parameters required: "value" "inital basis" "target basis"')
    sys.exit()

value, from_basis, to_basis = args[1], int(args[2]), int(args[3])
# value = input('Ausgangszahl: ')
res = convert(value, from_basis, to_basis)
print(res)
