#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 15:23:56 2020

@author: robin
"""


def fill(s, fill_with='0', length=8, left=True):
    placement = '>' if left else '<'
    return f'{s:{fill_with}{placement}{length}}'


def split_by_index(string, pos):
    try:
        pos.append(None)
    except AttributeError:
        pos = list(pos)
        pos.append(None)
    lastpos = 0
    res = []
    for p in pos:
        res.append(string[lastpos:p])
        lastpos = p
    return res
